import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/HelloWorld'
import Products from '@/components/Products'
import Producto from '@/components/Product'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello,
    },
    {
      path: '/products',
      name: 'Products',
      component: Products
    },
    {
      path: '/product/:id',
      name: 'Product',
      component: Producto
    }
  ]
})
